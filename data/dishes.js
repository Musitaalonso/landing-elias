export const starters = [
  {
    title: 'Rollos de primavera fritos',
    price: 2.95,
    details: 'pollo o vegetales'
  },
  {
    title: 'Gai Of Nuur Satay',
    price: 4.95,
    details: 'pollo o carne en pinchos con salsa de cacahuete'
  },
  {
    title: 'Tofu Tod',
    price: 3.95,
    details: 'tofu frito con una salsa de cacahuete y chili suave'
  },
  {
    title: 'Rollito de Verano Tailandés Fresco',
    price: 4.50,
    details: 'con camarón en salsa de tamarindo'
  },
  {
    title: 'Rollos de camarón tigre fritos',
    price: 22,
    details: 'con una salsa de ciruela'
  },
  {
    title: 'Costillas a la tailandesa',
    price: 8.95,
    details: 'costillas de cerdo glaseadas a la parrilla'
  }
]

export const mains = [
  {
    title: 'Gai Pad Khing',
    price: 7.95,
    details: 'pollo en rebanadas salteado con hongos, pimientos, calabaza, zanahorias y cebollas en salsa de jengibre fresco'
  },
  {
    title: 'Nuur Kra Prow',
    price: 8.50,
    details: 'carne salteada en rebanadas con pimientos, calabaza, zanahorias y cebollas en una salsa de chili y albahaca'
  },
  {
    title: 'Koong Pad Khing',
    price: 8.95,
    details: 'camarón salteado con hongos, pimientos, calabaza, zanahorias y cebollas en salsa de jengiblre fresco'
  },
  {
    title: 'Gai Kratiam',
    price: 7.95,
    details: 'pollo en rebanadas salteado con brócoli, calabaza y cebollas en salsa de ajo fresco'
  },
  {
    title: 'Carne y brócoli',
    price: 8.50,
    details: 'salteado con salsa de ostras'
  },
  {
    title: 'Koong Kratiam',
    price: 8.95,
    details: 'camarón salteado con brócoli, calabaza, zanahorias y cebollas en una salsa de ajo fresco'
  },
  {
    title: 'Gai Kra Prow',
    price: 7.95,
    details: 'pollo salteado en rebanadas con pimientos, calabaza, zanahorias y cebollas en una salsa de chili y albahaca'
  },
  {
    title: 'Gai Or Moo Prik Khing',
    price: 8.95,
    details: 'pollo o cerdo en rebanadas salteado y judías verdes con pasta de curry rojo'
  },
  {
    title: 'Koong Kra Prow',
    price: 8.95,
    details: 'camarón salteado con pimientos, calabaza, zanahorias y cebollas en salsa de albahaca fresca'
  },
  {
    title: 'Pollo y brócoli',
    price: 7.95,
    details: 'salteado con salsa de ostras'
  },
  {
    title: 'Moo Kratiam',
    price: 7.95,
    details: 'cerdo salteado en rebanadas con brócoli, calabaza, zanahorias y cebollas en salsa de ajo fresco'
  },
  {
    title: 'Pla Muk Kratiam',
    price: 8.50,
    details: 'calamar salteado con brócoli, calabaza, zanahorias y cebollas en salsa de ajo fresco'
  },
  {
    title: 'Nuur Kratiam',
    price: 8.50,
    details: 'cerdo salteado en rebadas con brócoli, calabaza, zanahorias y cebollas en salsa de ajo fresco'
  },
  {
    title: 'Moo Kra Prow',
    price: 7.95,
    details: 'cerdo salteado en rebanadas con pimientos, calabaza, zanahorias y cebolla en salsa de chili y albahaca'
  },
  {
    title: 'Pla Muk Kra Prow',
    price: 8.50,
    details: 'calamar salteado con pimientos, calabaza, zanahorias y cebollas en salsa de chili y albahaca'
  }
]

export const desserts = [
  {
    title: 'Khao Neow Ma Muang',
    price: 5.95,
    details: 'mango en rebanadas con arroz pegajoso y leche de coco'
  },
  {
    title: 'Khao Tom Mud',
    price: 6.50,
    details: 'arroz pegajoso en hojas de plátano'
  },
  {
    title: 'Plátano Frito',
    price: 7.50,
    details: 'plátano frito con helado de coco'
  },
  {
    title: 'Khao Neow Dam',
    price: 5.50,
    details: 'arroz pegajoso negro con coco rallado'
  },
  {
    title: 'Nam Kang Sai',
    price: 6.95,
    details: 'hielo con jalea de hierba y nanjea'
  },
  {
    title: 'Helado',
    price: 4.95,
    details: 'sabores de estación preparados por nosotros'
  }
]
