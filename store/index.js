import Vuex from 'vuex'
import { starters, mains, desserts } from '~/data/dishes'

const createStore = () => {
  return new Vuex.Store({
    state: {
      starters,
      mains,
      desserts
    }
  })
}

export default createStore
